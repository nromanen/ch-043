package vanytate.test;

import static vanytate.util.InputOutput.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import vanytate.*;
import vanytate.Telephones;
import vanytate.util.DateFormat;
import vanytate.util.JsonIO;

public class Main {
	
	public static List<User> generateUser() {
		List<User> users = new ArrayList<>();
		Person person = new Person("Clarkson", "Vasilie", DateFormat.NORMAL.getDateTime("2012-1-12"));
		Address address = new Address(45678, "Moskow", "Valis", "345a", 345);
		Telephones telephones = new Telephones("213452345", "5234521345", "3452346346", "834560273083523");
		users.add(new User(person, address, telephones, "vasia@i.ua"));
		
		person = new Person("Kolas", "Ivan", DateFormat.NORMAL.getDateTime("2012-1-12"));
		address = new Address(456, "Chernivtsi", "Bukovinsika", "342a", 34);
		telephones = new Telephones("0372678954", "78871214", "380127964587", "2835235034", "893543463456346");
		users.add(new User(person, address, telephones, "ivanaon@gmail.com"));
		return users;
	}

	public static void main(String... args) {
//		File file = new File("src/xml/book.xml");
		File fileJson = new File("src/data/phoneBook.json");
		PhoneBook phoneBook = new PhoneBook();
		Group group = new Group(TypeOfGroup.CLASSMATE);
		group.addUsers(generateUser());
		phoneBook.addGroup(group);
		group = new Group(TypeOfGroup.FAMILY);
		group.addUsers(generateUser());
		phoneBook.addGroup(group);
//		System.out.println(phoneBook);
//		marshal(phoneBook, file);
//		unmarshal(file).printGroups();
//		JsonIO.marshal(phoneBook, fileJson);
		p(JsonIO.unmarshal(PhoneBook.class, fileJson));
//		p(null);
		
	}
	
//	public static PhoneBook unmarshal(File file) {
//		JAXBContext jaxbContext;
//		PhoneBook phoneBook = null;
//		try {
//			jaxbContext = JAXBContext.newInstance(PhoneBook.class);
//			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
//			phoneBook = (PhoneBook) jaxbUnmarshaller.unmarshal(file);
//		} catch (JAXBException e) {
//			e.printStackTrace();
//		}
//		return phoneBook;
//	}
//
//	public static void marshal(PhoneBook phoneBook, File file) {
//		JAXBContext jaxbContext;
//		try {
//			jaxbContext = JAXBContext.newInstance(PhoneBook.class);
//			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
//			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//			jaxbMarshaller.marshal(phoneBook, file);
//		} catch (JAXBException e) {
//			e.printStackTrace();
//		}
//	}

}
