package vanytate.test;

import static vanytate.util.InputOutput.*;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import vanytate.*;
import vanytate.util.DateFormat;
import vanytate.util.JsonIO;
import vanytate.util.XmlIO;
import vanytate.validation.EnDateValidator;
import vanytate.validation.UaDateValidator;
import vanytate.validation.Validators;

public class ConsoleTest {
	public static void main(String[] args) throws Exception {
		File fileXML = new File("src/data/phoneBook.xml");
		File fileJSON = new File("src/data/phoneBook.json");
		PhoneBook phoneBook = XmlIO.unmarshal(fileXML);
		int j = 0;
		Integer format = 1;
		DateFormat dateFormat;
		while(true) {
			format = inputInt("Enter format(EN - 1 or UA - 2): ");
			if (format == 2) {
				dateFormat = DateFormat.getDateFormat(format);
				dateFormat.setValidator(new EnDateValidator());
				break;
			} else if (format == 1) {
				dateFormat = DateFormat.getDateFormat(format);
				dateFormat.setValidator(new UaDateValidator());
				break;
			}
		}
		
		for (TypeOfGroup i: TypeOfGroup.values()) {
			p(i + " - " + j++);
		}		
		
		while(true) {
			TypeOfGroup groupType = getType();
			if (groupType == null) break;
			
			String lastName = getName("Last name: ");
			String firstName = getName("First name: ");
			String birthday = inputString(dateFormat.getFormat() + ": ");
			Person person = new Person(lastName, firstName, dateFormat.getDateTime(birthday));
			
			String homeNumber = getHomeWorkNumber("Home number: ");
			String workNumber = getHomeWorkNumber("Work number: ");
			Set<String> mobileNumbers = getMobileNumber("Mobile number: ");
			Telephones telephones = new Telephones(homeNumber, workNumber, mobileNumbers);
			String email = getEmail();
			int zipCode = getZipCode();
			String city = getTitle("City: ");
			String street = getTitle("Street: ");
			String house = getHouseNumber();
			int flat = inputInt("Flat: ");
			Address address = new Address(zipCode, city, street, house, flat);
			User user = new User(person, address, telephones, email);
			phoneBook.addUser(groupType, user);
		}
		XmlIO.marshal(phoneBook, fileXML);
		JsonIO.marshal(phoneBook, fileJSON);
		p(phoneBook);
	}
	
	private static String getName(String text) {
		String value;
		while(true) {
			value = inputString(text);
			if (Validators.isName(value)) {
				break;
			} else {
				pvl("(is not valid): ");
			}
		}
		return value;
	}
	
	private static String getTitle(String text) {
		String value;
		while(true) {
			value = inputString(text);
			if (Validators.isTitle(value)){
				break;
			} else {
				pvl("(is not valid): ");
			}
		}
		return value;
	}
	
	private static String getHomeWorkNumber(String text) {
		String value;
		while(true) {
			value = inputString(text);
			if (Validators.isNewLine(value) || Validators.isHomeOrWorkNumber(value)) {
				break;
			} else {
				pvl("(is not valid): ");
			}
		}
		return value;
	}
	
	private static Set<String> getMobileNumber(String text) {
		Set<String> values = new HashSet<>();
		String temp;
		while(true) {
			temp = inputString(text);
			if (Validators.isNewLine(temp)) break;
			if (Validators.isMobileNumber(temp)) {
				values.add(temp);
			} else {
				pvl("(is not valid): ");
			}
		}
		return values;
	}
	
	private static int getZipCode() {
		String zip;
		while(true) {
			zip = inputString("Zip Code: ");
			if (Validators.isZipCode(zip)) break;
			else pvl("(is not valid): ");
		}
		return Integer.parseInt(zip);
	}
	
	private static String getEmail() {
		String value;
		while(true) {
			value = inputString("Email: ");
			if (Validators.isEmail(value)) {
				break;
			} else {
				pvl("(is not valid): ");
			}
		}
		return value;
	}
	
	private static TypeOfGroup getType() {
		String type;
		Integer number = null;
		while (true) {
			type = inputString("Enter group: ");
			if (Validators.isNewLine(type)) break;
			if (Validators.isNumber(type)) {
				number = Integer.parseInt(type);
			} else {
				pvl("Enter number");
				continue;
			}
			if (number < TypeOfGroup.count()) {
				return TypeOfGroup.getType(number);
			} else {
				pvl("This group not exist!\nTry again: ");
			}
		}
		return null;
	}
	
	private static String getHouseNumber() {
		String value;
		while(true) {
			value = inputString("House's number: ");
			if (Validators.isHouseNumber(value)) {
				break;
			} else {
				pvl("(is not valid): ");
			}
		}
		return value;
	}
	
}
