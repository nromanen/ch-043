package vanytate.util;

import java.io.IOException;
import org.joda.time.format.DateTimeFormatter;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class CustomDateSerializer extends JsonSerializer<DateTime> {

	  private static DateTimeFormatter formatter = 
		        DateTimeFormat.forPattern("yyyy-MM-dd");
	
	@Override
	public void serialize(DateTime value, JsonGenerator generator, SerializerProvider arg2) throws IOException, JsonProcessingException {
		generator.writeString(formatter.print(value));	
	}

}
