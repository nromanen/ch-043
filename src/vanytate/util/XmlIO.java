package vanytate.util;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import vanytate.PhoneBook;

public class XmlIO {
	
	public static void marshal(PhoneBook phoneBook, File file) {
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(PhoneBook.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(phoneBook, file);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	public static PhoneBook unmarshal(File file) {
		JAXBContext jaxbContext;
		PhoneBook phoneBook = null;
		try {
			jaxbContext = JAXBContext.newInstance(PhoneBook.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			phoneBook = (PhoneBook) jaxbUnmarshaller.unmarshal(file);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return phoneBook;
	}
	
}
