package vanytate.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import vanytate.validation.Validator;

public enum DateFormat {

	NORMAL("yyyy-MM-dd"), EN("MM/dd/yyyy"), UA("dd.MM.yyyy");
	
	private String type;
	
	private Validator validator;
	
	private DateFormat(String type) {
		this.type = type;
	}
	
	public static DateFormat getDateFormat(int index) {
		return DateFormat.values()[index];
	}
	
	public DateTime getDateTime(String date) {
		if (validator != null) {
			if(!validator.check(date)) return null;
		}
		DateTimeFormatter dtf = DateTimeFormat.forPattern(type);
		return dtf.parseDateTime(date);
	}
	
	public void setValidator(Validator validator) {
		this.validator = validator;
	}
	
	public String getFormat() {
		return type;
	}
	
	@Override
	public String toString() {
		return super.toString();
	}
	
	public static void main(String[] args) {
		DateFormat s = DateFormat.NORMAL;
		System.out.println(s.getDateTime("2101-10-10"));
	}
}
