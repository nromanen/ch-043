package vanytate.util;


import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateTimeAdapter extends XmlAdapter<String, DateTime> {

	private final DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd");
	
	@Override
	public String marshal(DateTime v) throws Exception {
		synchronized(dtf) {
			return dtf.print(v);
		}
	}
	
	@Override
	public DateTime unmarshal(String v) throws Exception {
		synchronized(dtf) {
			return dtf.parseDateTime(v);
		}
	}
	
}
