package vanytate.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class InputOutput {

	public static void p(Object... objects) {
		for (Object obj: objects) {
			System.out.println(obj);
		}
	}
	
	public static void pvl(Object... objects) {
		for (Object obj: objects) {
			System.out.print(obj);
		}
	}
	
	public static Integer inputInt(String s) {
		pvl(s);
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		Integer value = null;
		while(true) {
			try {
				String sc = stdin.readLine();
				value = Integer.parseInt(sc);
				break;
			} catch (NumberFormatException e) {
				p();
				pvl("is not int, try again: ");
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
		return value;
	}
	
	public static Integer inputInt() {
		return inputInt("");
	}
	
	public static String inputString(String s) {
		pvl(s);
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		String value = null;
		try {
			value = (stdin.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return value;
	}
	
	public static String inputString() {
		return inputString("");
	}
	
	public static void main(String[] args) {
		inputInt();
	}
}
