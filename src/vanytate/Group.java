package vanytate;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "group")
@XmlAccessorType(XmlAccessType.FIELD)
public class Group {

    private static long count;
    
    @XmlAttribute
    private final long id = count++;

	private TypeOfGroup groupType;
	
	@XmlElement(name = "user")
	private List<User> users;
	
	public Group(TypeOfGroup groupType) {
		this.groupType = groupType;
		users = new ArrayList<>();
	}
	
	public Group(TypeOfGroup groupType, List<User> users) {
		this.groupType = groupType;
		this.users = users;
	}
	
	public Group(TypeOfGroup groupType, User... users) {
		this.groupType = groupType;
		this.users = new ArrayList<>();
		Collections.addAll(this.users, users);
		
	}

    public long getId() {
		return id;
	}

	public TypeOfGroup getGroupType() {
        return groupType;
    }

    public List<User> getUsers() {
		return users;
	}
	
	public void setUsers(List<User> users) {
		this.users = users;
	}

	public void addUsers(Collection<User> users) {
		this.users.addAll(users);
	}
	
	public void addUsers(User... users) {
		Collections.addAll(this.users, users);
	}
	
	public void removerUser(User user) {
		users.remove(user);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (obj instanceof Group) {
			Group anGroup = (Group) obj;
			if (groupType == anGroup.groupType || users.equals(anGroup.users)) {
				return true;
			}
		}
		return false;
	}
	
	public void print() {
		users.forEach(System.out::println);
	}
	
	public void sortByFirstName() {
		Collections.sort(users, (o1, o2) -> {
			return o1.getPerson().getFirstName().compareTo(o2.getPerson().getFirstName());
		});
	}
	
	public void sortByLastName() {
		Collections.sort(users, (o1, o2) -> {
			return o1.getPerson().getLastName().compareTo(o2.getPerson().getLastName());
		});
	}
	
	private boolean isExistUser(User anUser) {
		for (User user: users) {
			if (user.equals(anUser)) return true;
		}
		return false;
	}
	
	private Group() {};
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		users.forEach(e -> {
			sb.append(e);
			sb.append("\n\n");
		});
		return sb.toString();
	}
	
}
