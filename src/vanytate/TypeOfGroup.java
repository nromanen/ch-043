package vanytate;

public enum TypeOfGroup {
	FAMILY, FRIEND, WORK,
	NEIGHBOR, CLASSMATE;
	
	public static TypeOfGroup getType(int k) {
		return TypeOfGroup.values()[k];
	}
	
	public static int count() {
		return TypeOfGroup.values().length;
	}
}
