package vanytate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
	
	private static long count = 0;
	
	@XmlAttribute
	private long id = count++;
	
	private Person person;
	private Address address;
	private Telephones telephones;
	private String email;
	
	public User(Person person, Address address, Telephones telephones, String email) {
		this.person = person;
		this.address = address;
		this.telephones = telephones;
		this.email = email;
	}
	
	private User() {}
	
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Telephones getTelephones() {
		return telephones;
	}
	public void setTelephones(Telephones telephones) {
		this.telephones = telephones;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return person + " \n" + email + " " + address + "\n" + telephones;
	}
	
}
