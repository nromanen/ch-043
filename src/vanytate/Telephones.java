package vanytate;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.codehaus.jackson.annotate.JsonIgnore;

@XmlAccessorType(XmlAccessType.FIELD)
public class Telephones {
	
    private String homeNumber;
    private String workNumber;
    
    @XmlElementWrapper(name = "mobileNumbers")
    @XmlElement(name = "mobileNumber")
	private Set<String> mobileNumbers;
	
	public Telephones(String homeNumber, String workNumber, Set<String> mobileNumbers) {
		this.homeNumber = homeNumber;
		this.workNumber = workNumber;
		this.mobileNumbers = mobileNumbers;
	}
	
	public Telephones(String homeNumber, String workNumber, String... mobileNumbers) {
		this.homeNumber = homeNumber;
		this.workNumber = workNumber;
		this.mobileNumbers = new HashSet<String>();
		for (String i: mobileNumbers) {
			this.mobileNumbers.add(i);
		}
	}

	private Telephones() {}
	
	public String getHomeNumber() {
		return homeNumber;
	}
	
	@JsonIgnore
	public String getHomeNumberShortForm() {
		int len = homeNumber.length();
		if (len > 6) {
			return homeNumber.substring(len - 6, len);
		} else {
			return homeNumber;
		}
	}
	
	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}
	
	public String getWorkNumber() {
		return workNumber;
	}
	
	@JsonIgnore
	public String getWorkNumberShortForm() {
		int len = workNumber.length();
		if (len > 6) {
			return workNumber.substring(len - 6, len);
		}
		return workNumber;
	}
	
	public void setWorkNumber(String workNumber) {
		this.workNumber = workNumber;
	}
	
	public Set<String> getMobileNumbers() {
		return mobileNumbers;
	}
	
	public void setMobileNumbers(Set<String> mobileNumbers) {
		this.mobileNumbers = mobileNumbers;
	}
	
	public void addMobileNumber(String... numbers) {
		for (String i: numbers) {
			mobileNumbers.add(i);
		}
	}
	
	@Override
	public String toString() {
		return getHomeNumberShortForm() + ", " + getWorkNumberShortForm() + ", " + String.join(", ", mobileNumbers);
	}
	
	public static void main(String[] args) {
		System.out.println(new Telephones("1232523", "42523454", "35245234"));
	}
}
