package vanytate;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

@XmlRootElement
public class Address {

    private int zipCode;

    private String city;

    private String street;

    private String house;

    private int flat;

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public int getFlat() {
		return flat;
	}

	public void setFlat(int flat) {
		this.flat = flat;
	}
	
	private Address() {}

	public Address(int zipCode, String city, String street, String house, int flat) {
        this.zipCode = zipCode;
        this.city = city;
        this.street = street;
        this.house = house;
        this.flat = flat;
    }

    @Override
    public String toString() {
    	return getFullInfo().substring(0, 10);
    }
    
    @JsonIgnore
    public String getFullInfo() {
    	return zipCode + " " + city + " " + street + " " + house + " " + flat;
    }
}
