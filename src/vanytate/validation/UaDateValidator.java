package vanytate.validation;

import java.util.regex.Pattern;

public class UaDateValidator implements Validator {

	private static final Pattern VALID_UA_DATE = 
			Pattern.compile("^([1-2]\\d|3[01]|0?[1-9])\\.(0?[1-9]|1[012])\\.(19|2\\d)\\d\\d$");
	
	public static boolean isUaFormatDate(String name) {
		return VALID_UA_DATE.matcher(name).matches();
	}
	
	@Override
	public boolean check(String text) {
		return isUaFormatDate(text);
	}

}
