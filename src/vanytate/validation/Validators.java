package vanytate.validation;

import static vanytate.util.InputOutput.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

public class Validators {

	private static final Pattern VALID_EMAIL = 
			Pattern.compile("^[A-z0-9-\\+\\._]+@[A-z0-9]+((-|\\.)[A-z0-9]+)*\\.[A-z0-9]+((-|\\.)[A-z0-9]+)*[A-z\\d]+$");
	
	private static final Pattern VALID_NAME = 
			Pattern.compile("^([A-Z][a-z]*(-[A-Z])?[a-z]*)|([À-ß²¯ª][à-ÿ³¿º]*(-[À-ß²¯ª])?[à-ÿ³¿º]*)$"); 

	private static final Pattern VALID_EN_DATE = 
			Pattern.compile("^(0?[1-9]|1[012])/([1-2]\\d|3[01]|0?[1-9])/(19|2\\d)\\d\\d");
	
	private static final Pattern VALID_UA_DATE = 
			Pattern.compile("^([1-2]\\d|3[01]|0?[1-9])\\.(0?[1-9]|1[012])\\.(19|2\\d)\\d\\d$");
	
	private static final Pattern VALID_MOBILE_NUMBER = 
			Pattern.compile("^0\\d{9}$");
	
	private static final Pattern VALID_HOME_WORK_NUMBER = 
			Pattern.compile("^\\d{9}$");
	
	private static final Pattern VALID_NEW_LINE = 
			Pattern.compile("^(\\W|$)", Pattern.DOTALL);
	
	private static final Pattern VALID_ZIP_CODE = 
			Pattern.compile("^\\d{1,5}$");
	
	private static final Pattern VALID_NUMBER_OF_HOUSE =
			Pattern.compile("^\\d+[A-z]?$");
	
	private static final Pattern VALID_NUMBER =
			Pattern.compile("^\\d+$");
	
	public static boolean isEmail(String email) {
		return VALID_EMAIL.matcher(email).matches();
	}
	
	public static boolean isName(String name) {
		if (name.length() > 10) return false;
		return VALID_NAME.matcher(name).matches();
	}
	
	public static boolean isTitle(String name) {
		return VALID_NAME.matcher(name).matches();
	}
	
	public static boolean isEnFormatDate(String name) {
		return VALID_EN_DATE.matcher(name).matches();
	}
	
	public static boolean isUaFormatDate(String name) {
		return VALID_UA_DATE.matcher(name).matches();
	}
	
	public static boolean isMobileNumber(String number) {
		return VALID_MOBILE_NUMBER.matcher(number).matches();
	}
	
	public static boolean isHomeOrWorkNumber(String number) {
		return VALID_HOME_WORK_NUMBER.matcher(number).matches();
	}
	
	public static boolean isNewLine(String text) {
		return VALID_NEW_LINE.matcher(text).matches();
	}
	
	public static boolean isZipCode(String code) {
		return VALID_ZIP_CODE.matcher(code).matches();
	}
	
	public static boolean isHouseNumber(String numb) {
		return VALID_NUMBER_OF_HOUSE.matcher(numb).matches();
	}
	
	public static boolean isNumber(String numb) {
		return VALID_NUMBER.matcher(numb).matches();
	}
	
	public static void main(String[] args) throws IOException {
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		final String check = "123456089";
		p(VALID_EN_DATE.matcher(check));
		p(check);
		while(true)
		p(isUaFormatDate(stdin.readLine()));
	}
}
