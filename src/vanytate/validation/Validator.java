package vanytate.validation;

@FunctionalInterface
public interface Validator {
	
	boolean check(String text);
}
