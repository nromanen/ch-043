package vanytate.validation;

import java.util.regex.Pattern;

public class EnDateValidator implements Validator {

	private static final Pattern VALID_EN_DATE = 
			Pattern.compile("^(0?[1-9]|1[012])/([1-2]\\d|3[01]|0?[1-9])/(19|2\\d)\\d\\d");
	
	public static boolean isEnFormatDate(String name) {
		return VALID_EN_DATE.matcher(name).matches();
	}
	
	@Override
	public boolean check(String text) {
		return isEnFormatDate(text);
	}

}
