package vanytate;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PhoneBook {

	@XmlElement(name = "group")
	@JsonProperty("groups")
    private List<Group> groups;

    public PhoneBook() {
        groups = new ArrayList<Group>();
    }
    
    public PhoneBook(TypeOfGroup... types) {
    	this();
    	for (TypeOfGroup i: types) {
    		groups.add(new Group(i));
    	}
    }

    public void addGroup(Group... newGroups) {
    	for(Group group: newGroups) {
			Group g = getGroup(group.getGroupType());
			if (g == null) {
				this.groups.add(group);
			} else {
				g.addUsers(group.getUsers());
			}    		
    	}
    }
    
    public void addUser(TypeOfGroup groupType, User user) {
    	Group group = getGroup(groupType);
    	if (group == null) {
    		group = new Group(groupType, user);
    		this.groups.add(group);
    	} else {
    		group.addUsers(user);
    	}
    }
    
    public Group getGroup(TypeOfGroup groupType) {
    	for (Group group: groups) {
    		if (group.getGroupType().equals(groupType)) return group;
    	}
    	return null;
    }
    
    public User getUsersByLastName(String lastName) {
    	for (Group group: this.groups) {
    		for (User user: group.getUsers()) {
    			if (lastName.equals(user.getPerson().getLastName())) return user;
    		}
    	}
    	return null;
    }
    
    @Override
    public String toString() {
		StringBuilder sb = new StringBuilder();
		groups.forEach(sb::append);
		return sb.toString();
    }
}
