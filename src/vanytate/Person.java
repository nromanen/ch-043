package vanytate;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Person {
	
	private String lastName;
	private String firstName;
	
	@JsonSerialize(using = vanytate.util.CustomDateSerializer.class)
	@XmlJavaTypeAdapter(vanytate.util.DateTimeAdapter.class)
	private DateTime birthday;
	
	public Person(String lastName, String firstName, DateTime birthday) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.birthday = birthday;
	}

	private Person() {}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

    public DateTime getBirthday() {
        return birthday;
    }
    
    @JsonIgnore
    public String getEnFormatBirthday() {
    	DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy");
    	return dtf.print(birthday);
    }
    
    public void setBirthday(DateTime birthday) {
        this.birthday = birthday;
    }

	@Override
	public String toString() {
		return lastName + " " + firstName + " (" + getEnFormatBirthday() + ")";
	}
}
